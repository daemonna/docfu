KEYCHAR = "@"

DOCTYPE = {
    ascii,
    
}


article	For articles in scientific journals, presentations, short reports, program documentation, invitations, ...
IEEEtran	For articles with the IEEE Transactions format.
proc	A class for proceedings based on the article class.
minimal	It is as small as it can get. It only sets a page size and a base font. It is mainly used for debugging purposes.
report	For longer reports containing several chapters, small books, thesis, ...
book	For books.
slides	For slides. The class uses big sans serif letters.
memoir	For changing sensibly the output of the document. It is based on the book class, but you can create any kind of document with it [1]
letter	For writing letters.
beamer	For writing presentations (see LaTeX/Presentations).