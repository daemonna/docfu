struct Document {
    id: i64,
    doctype: i32,
    template: String,
    vecs: Vec<f32>
}

// Half title	Publisher	A mostly-blank page at the front of the book block which precedes the title page and contains only the title (omitting the subtitle, author, publisher, etc. found on the full title page) and occasionally includes some slight ornamentation. The title traditionally appears on the page as a single line in capital letters, but modern half title pages may be scaled-down versions of the typography from the full title page. The half title page faces a blank verso or an endpaper.
// Frontispiece	Author or publisher	A decorative illustration on the verso facing the title page. It may be an image related to the book's subject or a portrait of the author. Frontispieces have become less common, with a list of the author's previous works or other titles in a multi-author series frequently taking the place of the frontispiece.
// Title page	Publisher	Repeats the title and author as printed on the cover or spine, often using typographic elements carried over from either the cover design or from the rest of the book's interior. Title pages may also include the publisher's logo accompanied by the city and/or year of publication.
// Colophon	Publisher and printer	Technical information such as edition dates, copyrights, typefaces and translations used, and the name and address of the publisher or printer. It usually appears in modern books on the verso of the title page, but in some books is placed at the end (see Back matter). Lengthy colophons for books collecting material from multiple copyrighted works may continue onto pages in the back matter if it will not fit on a single page. Also known as the Edition notice or Copyright page.
// Dedication	Author	A dedication page is a page in a book that precedes the text, in which the author names the person or people for whom he/she has written the book.
// Epigraph	Author	A phrase, quotation, or poem. The epigraph may serve as a preface, as a summary, as a counter-example, or to link the work to a wider literary canon, either to invite comparison, or to enlist a conventional context.
// Table of contents	Publisher	This is a list of chapter headings and sometimes nested subheadings, together with their respective page numbers. This includes all front-matter items listed below, together with chapters in the body matter and back matter. The number of levels of subheadings shown should be limited, so as to keep the contents list short, ideally one page, or possibly a double-page spread. Technical books may include a list of figures and a list of tables.
// Foreword	Some person other than the author	Often, a foreword will tell of some interaction between the writer of the foreword and the writer of the story, or a personal reaction and significance the story elicited. A foreword to later editions of a work often describes the work's historical context and explains in what respects the current edition differs from previous ones.
// Preface	Author	A preface is generally the author recounting the story of how the book came into being, or how the idea for the book was developed. This is often followed by thanks and acknowledgments to people who were helpful to the author during the time of writing.
// Acknowledgments	Author	Sometimes part of the preface rather than a separate section in its own right, or sometimes placed in the back matter rather than the front, it acknowledges those who contributed to the creation of the book.
// Introduction	Author	A beginning section which states the purpose and goals of the following writing.
// Prologue	Narrator (or a character in the book)	Usually present in fiction or narrative nonfiction, the prologue is an opening to the story that establishes the setting and gives background details, often from some earlier or later timeframe that ties into the main one. As such, it is generally considered part of the body in modern book organization (cf. Chicago Manual of Style).

impl Book {
    fn new(cols: i32, rows: i32) -> Matrix {
        Matrix {
            cols: cols,
            rows: rows,
            data: vec![0.0; cols * rows]
        }
    }
}


// Volumes
// A volume is a set of leaves bound together. Thus each work is either a volume, or is divided into volumes.
// Books and parts
// Single-volume works account for most of the non-academic consumer market in books. A single volume may embody either a part of a book or the whole of a book; in some works, parts encompass multiple books, while in others, books may consist of multiple parts.
// Chapters and sections
// A chapter or section may be contained within a part or a book. When both chapters and sections are used in the same work, the sections are more often contained within chapters than the reverse.
// Modules and units
// In some books the chapters are grouped into bigger parts, sometimes called modules. The numbering of the chapters can begin again at the start of every module. In educational books, especially, the chapters are often called units.



// Epilogue	The narrator (or a character in the book)	This piece of writing at the end of a work of literature or drama is usually used to bring closure to the work.
// Extro or Outro		The conclusion to a piece of work; this is considered the opposite of the intro. These terms are more commonly used in music.
// Afterword	The author, or some other real person	An afterword generally covers the story of how the book came into being, or of how the idea for the book was developed.
// Conclusion	Author	
// Postscript		
// Appendix or Addendum	Author	This supplemental addition to a given main work may correct errors, explain inconsistencies or otherwise detail or update the information found in the main work.
// Glossary	Author	The glossary consists of a set of definitions of words of importance to the work. They are normally alphabetized. The entries may consist of places and characters, which is common for longer works of fiction.
// Bibliography	Author	This cites other works consulted when writing the body. It is most common in non-fiction books or research papers.
// Index	Publisher	This list of terms used in the text contains references, often page numbers, to where the terms can be found in the text. Most common in non-fiction books.
// Colophon	Publisher	This brief description may be located at the end of a book or on the verso of the title page. It describes production notes relevant to the edition and may include a printer's mark or logotype.
// Postface		A postface is the opposite of a preface, a brief article or explanatory information placed at the end of a book.[1] Postfaces are quite often used in books so that the non-pertinent information will appear at the end of the literary work, and not confuse the reader.